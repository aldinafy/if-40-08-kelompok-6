# import library socket karena menggunakan IPC socket
import socket

# definisikan IP untuk binding
address = "192.168.1.18"

# definisikan port untuk binding
port = 3305

# definisikan ukuran buffer untuk menerima pesan
bufferSize = 1024

# buat socket (bertipe UDP atau TCP?)
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# lakukan binding ke IP dan port
s.bind((address, port))

# lakukan listen
s.listen()

#  siap menerima koneksi
c, addr = s.accept()

print ('Connection address:', addr)

# buka/buat file bernama hasil_upload.txt untuk menyimpan hasil dari file yang dikirim server
# masih hardcoded nama file, bertipe byte
f = open('hasil_upload.txt', 'w', bufferSize)

# loop forever
while 1:
    # terima pesan dari client
    data = c.recv(bufferSize)
    
    # tulis pesan yang diterima dari client ke file kita (result.txt)
    f.write(data.decode('utf-8', 'strict'))
    
    # berhenti jika sudah tidak ada pesan yang dikirim
    if not data: break
    
# tutup file result.txt    
f.close()

#tutup socket
s.close()

# tutup koneksi
c.close()