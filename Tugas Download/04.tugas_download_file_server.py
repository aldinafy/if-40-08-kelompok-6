# import library socket karena menggunakan IPC socket
import socket

# definisikan IP untuk binding
address = '192.168.1.18'

# definisikan port untuk binding
port = 3305

# definisikan ukuran buffer untuk menerima pesan
bufferSize = 1024

# buat socket (bertipe UDP atau TCP?)
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# lakukan binding ke IP dan port
s.bind((address, port))

# lakukan listen
s.listen()

#  siap menerima koneksi
c, addr = s.accept()


# buka file bernama "file_didownload.txt
# masih hard code, file harus ada dalam folder yang sama dengan script python
f = open('file_didownload.txt', 'rb')

try:
    # baca file tersebut sebesar buffer 
    byte = f.read(bufferSize)
    
    # selama tidak END OF FILE; pada pyhton EOF adalah b''
    while byte != b'':
        # kirim hasil pembacaan file dari server ke client
        c.send(byte)
        
        # baca sisa file hingga EOF
        byte = f.read(bufferSize)
        
finally:
    print ("end sending")
    
    # tutup file jika semua file telah  dibaca
    f.close()

# tutup socket
s.close()

# tutup koneksi
c.close()