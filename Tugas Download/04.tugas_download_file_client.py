# import library socket karena menggunakan IPC socket
import socket

# definisikan IP server tujuan file akan diupload
address = "127.0.0.1"

# definisikan port number proses di server
port = 3305

# definisikan ukuran buffer untuk mengirim
bufferSize = 1024

# buat socket (apakah bertipe UDP atau TCP?)
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# lakukan koneksi ke server
s.connect((address, port))

# buka file bernama "hasil_download.txt bertipe byte
# masih hard code, file harus ada dalam folder yang sama dengan script python
f = open('hasil_didownload.txt', 'w')

# loop forever
while 1:
    # terima pesan dari client
    data = s.recv(bufferSize)
    
    # tulis pesan yang diterima dari client ke file kita (result.txt)
    f.write(data.decode('utf-8', 'strict'))
    
    # berhenti jika sudah tidak ada pesan yang dikirim
    if not data: break
    
# tutup file_hasil_download.txt    
f.close()

#tutup socket
s.close()