# import library socket karena akan menggunakan IPC socket
import socket

# definisikan alamat IP bind dari server
ip = "10.20.0.15"

# definisikan port number untuk bind dari server
port = 12345

# buat socket bertipe UDP
serverSock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

# lakukan bind
serverSock.bind((ip, port))

# loop forever
while True:
    # terima pesan dari client
    data, addr = serverSock.recvfrom(12345)
    
    # menampilkan hasil pesan dari client
    #print (addr)
    print("Message: ", data)
    
