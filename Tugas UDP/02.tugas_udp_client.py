# import library socket karena akan menggunakan IPC socket
import socket

# definisikan target IP server yang akan dituju
ip = "127.0.0.1"

# definisikan target port number server yang akan dituju
port = 12345

PESAN = 'Halo Server'

print ("target IP:", ip)
print ("target port:", port)
print ("pesan:", PESAN)

# buat socket bertipe UDP
clientSock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
# lakukan loop 10 kali
for x in range (10):
    # definisikan pesan yang akan dikirim
    clientSock.sendto(PESAN.encode(), (ip, port))
    # kirim pesan    
    