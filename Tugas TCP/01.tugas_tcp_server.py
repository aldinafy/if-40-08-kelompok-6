# import library socket karena akan menggunakan IPC socket
import socket

# definisikan alamat IP binding  yang akan digunakan 
address = "10.20.0.71"

# definisikan port number binding  yang akan digunakan 
port = 3304

# definisikan ukuran buffer untuk mengirimkan pesan
bufferSize = 1024

# buat socket bertipe TCP
s = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
print("Berhasil membuat koneksi socket pada", address)

# lakukan bind
s.bind((address, port))

# server akan listen menunggu hingga ada koneksi dari client
s.listen()
print("Mendengarkan koneksi pada port", port)

# lakukan loop forever
while 1:
	# menerima koneksi
    conn, addr = s.accept()
	
	# menampilkan koneksi berupa IP dan port client yang terhubung menggunakan print
    print("Mendapatkan koneksi dari ", addr)
	
	# menerima data berdasarkan ukuran buffer
    recdata = conn.recv(bufferSize)
	
	# menampilkan pesan yang diterima oleh server menggunakan print
    print("Data dari client ", str(recdata))
	
	# mengirim kembali data yang diterima dari client kepada client
    conn.send(recdata)

# tutup koneksi	
s.close()