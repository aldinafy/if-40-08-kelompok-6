# import library socket karena akan menggunakan IPC socket
import socket

# definisikan tujuan IP server
address = '127.0.0.1'

# definisikan port dari server yang akan terhubung
port = 3304

# definisikan ukuran buffer untuk mengirimkan pesan
bufferSize = 1024

# definisikan pesan yang akan disampaikan
pesan = b"barbar"

# buat socket TCP
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# lakukan koneksi ke server dengan parameter IP dan Port yang telah didefinisikan
s.connect((address, port))

# kirim pesan ke server
s.send(pesan)

# terima pesan dari server
recdata = s.recv(bufferSize)

# tampilkan pesan/reply dari server
print(recdata)

# tutup koneksi
s.close()